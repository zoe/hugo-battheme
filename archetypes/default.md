<title>{{ .Title }}</title>

{{ $css := resources.Get "sass/main.sass" }}
{{ $css = $css | toCSS }}

<link rel="stylesheet" href="{{ $css.RelPermalink }}"
